const jwt = require('jsonwebtoken');
const token = (user, secret, liveTime) => {
    return jwt.sign(
        {
            user
        },
        secret,
        {
            expiresIn: liveTime
        }
    );
}

const createTokens = (user, secret) => {
    return {
        accessToken: token(user, process.env.APP_SECRET, process.env.TOKEN_LIFETIME),
        refreshToken: token(user, process.env.APP_REFRASH_SECRET, process.env.REFRASH_TOKEN_LIFETIME)
    }
}

module.exports = { createTokens }

module.exports = ( sequelize, Type ) => {
    return sequelize.define("users",{
        id : {
            type : Type.UUID,
            primaryKey : true,
            unique : true,
            defaultValue : Type.UUIDV4,
        },
        userName : {
            type : Type.STRING
        },
        password: {
            type: Type.STRING
        }
    });
};

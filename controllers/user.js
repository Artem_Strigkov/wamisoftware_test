const db = require("../models");
const { createTokens } = require("../helper/token");

module.exports = {
    //=========================== USERS ===============================
    signUp(req, res, next) {
        const userName = req.body.userName;
        const password = req.body.password;

        const user = {
            userName, password
        }

        db.users.create(user)
            .then(result => {
                return res.status(200).json(result);
            })
            .catch(err => {
                return res.status(400).send(err);
            });
    },

    signIn(req, res, next) {
        const userName = req.body.userName;
        const password = req.body.password;


        db.users.findOne({
            where:{
                userName
            }
        })
            .then(async user => {
                if(password === user.password){
                    const token = await createTokens(user);
                    const userEx = user.dataValues;

                    await res.status(200).json({...userEx, accessToken: token.accessToken, refreshToken: token.refreshToken});
                return true;
                }
                return res.status(200).json("Password is incorrect");
            })
            .catch(err => {
                return res.status(400).send(err);
            });
    }
};

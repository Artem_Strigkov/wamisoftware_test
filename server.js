const express = require("express");
require("dotenv").config();
const db = require("./models");
const bodyParser = require("body-parser");

//Create app
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// CORS
app.use(function(req, res, next) {
    // Websites allowed to connect
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader("Content-Type", "application/json");
    next();
});

db.sequelize
    .sync(
        // {force:true},
        {logging: false}
    );

app.use("/api/v1", require("./routes/v1"));


app.listen(process.env.PORT, function() {
    console.log(`Node server listening on port ${process.env.PORT}`);
});

